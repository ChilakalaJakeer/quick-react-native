/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import FlexRowCentred from '../elements/FlexRowCentred';
import ActionButton from '../elements/ActionButton';
import {ScrollView} from 'react-native-gesture-handler';
import moment from 'moment';
import {ACTIVITY_LIST_KEY} from '../../config/async-storageKeys';
import {textColor, bgColor} from '../../config/Theme';

export default function Save(props) {
  const [actvityName, setActvityName] = useState('untitled');

  const route = useRoute();
  const {timeTaken} = route.params;
  let time = moment.utc(timeTaken).format('HH:mm:ss');
  const message = ['That was real quick !', 'It takes time to acheive big !'];

  const navigation = useNavigation();
  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  const save = async () => {
    let actvityList = await AsyncStorage.getItem(ACTIVITY_LIST_KEY);
    if (actvityList === null) {
      actvityList = [];
    } else {
      actvityList = JSON.parse(actvityList);
    }
    const timeStamp = new Date().getTime();
    const id = getRandomInt(9999999);
    const actvity = {id, actvityName, timeTaken, timeStamp};
    actvityList.unshift(actvity);
    await AsyncStorage.setItem(ACTIVITY_LIST_KEY, JSON.stringify(actvityList));
    console.log(JSON.stringify(actvityList));
  };

  return (
    <ScrollView style={{flex: 1}}>
      <View style={styles.root}>
        <FlexRowCentred endFlex={1} flex={1} height={150}>
          <Text style={styles.textMessage}>
            {timeTaken > 10000 ? message[1] : message[0]}
          </Text>
        </FlexRowCentred>
        <FlexRowCentred endFlex={1} flex={1} height={100}>
          <Text style={styles.textTime}>{time}</Text>
        </FlexRowCentred>

        <View style={{height: 400, flex: 1}}>
          <FlexRowCentred endFlex={0.1} flex={0.12}>
            <Text style={{fontSize: 24, paddingStart: 7}}>Actvity name</Text>
          </FlexRowCentred>

          <FlexRowCentred endFlex={0.1} flex={0} marginBottom={16}>
            <TextInput
              style={styles.inputBox}
              onChangeText={text => {
                console.log(text);

                return setActvityName(text);
              }}
            />
          </FlexRowCentred>

          <FlexRowCentred endFlex={0.1}>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-start',
              }}>
              <ActionButton
                width={100}
                height={50}
                bgColor={bgColor.secondary}
                action={'Clear'}
                elevation={7}
                borderRadius={7}
                onPress={() => setActvityName('')}
              />
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
              }}>
              <ActionButton
                width={100}
                height={50}
                bgColor={bgColor.primary}
                action={'Save'}
                borderRadius={7}
                elevation={7}
                onPress={() => {
                  save();
                  return navigation.navigate('activityTracker');
                }}
              />
            </View>
          </FlexRowCentred>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: bgColor.bg,
  },

  textMessage: {
    fontSize: 40,
    alignSelf: 'center',
    textAlign: 'center',
  },

  textTime: {
    fontSize: 60,
    alignSelf: 'center',
  },
  inputBox: {
    width: '100%',
    height: 50,
    borderColor: textColor.secondary,
    borderWidth: 1,
    borderRadius: 7,
    fontSize: 24,
    paddingStart: 24,
    paddingEnd: 24,
  },
});
