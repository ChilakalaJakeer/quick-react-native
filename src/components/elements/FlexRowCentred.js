/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View} from 'react-native';
export default function FlexRowCentred(props) {
  const {endFlex, style, ...rest} = props;
  //endFlex value = 0 to 1
  const childFlex = 1 - props.endFlex;
  return (
    <View
      style={[
        {
          flexDirection: 'row',
          ...rest,
        },
        style,
      ]}>
      <View style={{flex: endFlex}} />
      <View
        style={{
          flexDirection: 'row',
          flex: childFlex,
        }}>
        {props.children}
      </View>
      <View style={{flex: endFlex}} />
    </View>
  );
}
