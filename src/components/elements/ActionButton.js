import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import {bgColor} from '../../config/Theme';
export default function ActionButton(props) {
  return (
    <TouchableOpacity
      style={[
        styles.root,
        {
          width: props.width,
          height: props.height,
          backgroundColor: props.bgColor,
          borderRadius: props.borderRadius,
          ...props,
        },
      ]}
      onPress={props.onPress}>
      <View style={styles.viewStart}>
        <Text style={styles.textStart}>{props.action}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  root: {width: 90, height: 30, backgroundColor: 'lightgreen'},
  viewStart: {flex: 1, justifyContent: 'space-around', alignItems: 'center'},
  textStart: {fontSize: 30, fontWeight: '900', color: bgColor.bg},
});
