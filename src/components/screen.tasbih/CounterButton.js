import React from 'react';
import styled from 'styled-components';
import {bgColor, textColor} from '../../config/Theme';

const CounterBtn = styled.TouchableOpacity`
  background-color: ${bgColor.primary};
  width: 300;
  height: 300;
  border-radius: 150;
  elevation: 7;
`;

const BtnTitleContainer = styled.View`
  flex: 1;
  justify-content: center;
`;

const BtnTitle = styled.Text`
  font-size: 50;
  color: ${textColor.primary};
  text-align: center;
`;

export default function CounterButton({count, setCount, counter}) {
  return (
    <CounterBtn
      style={{
        backgroundColor:
          counter === count ? bgColor.primary : bgColor.secondary,
      }}
      onPress={() => setCount(count + 1)}>
      <BtnTitleContainer>
        <BtnTitle>Counter</BtnTitle>
      </BtnTitleContainer>
    </CounterBtn>
  );
}
