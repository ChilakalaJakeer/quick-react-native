import React from 'react';
import {bgColor} from '../../config/Theme';
import styled from 'styled-components';

const ResetBtn = styled.TouchableOpacity`
  background-color: ${bgColor.primary};
  opacity: 0.8;
  width: 100;
  height: 100;
  border-radius: 50;
  elevation: 5;
`;

const TitleView = styled.View`
  flex: 1;
  justify-content: center;
`;

const Title = styled.Text`
  font-size: 20;
  color: ${bgColor.bg};
  text-align: center;
`;

export default function ResetCount({setCount, setCounter}) {
  return (
    <ResetBtn
      onPress={() => {
        setCount(0);
        return setCounter(0);
      }}>
      <TitleView>
        <Title>Reset</Title>
      </TitleView>
    </ResetBtn>
  );
}
