import React, {useState} from 'react';

export const CountContext = React.createContext();

export default function CountContextProvider(props) {
  const [count, setCount] = useState(0);
  return (
    <CountContext.Provider value={{setCount, count}}>
      {props.children}
    </CountContext.Provider>
  );
}
