import React from 'react';
// import sliderThumb from '../sliderThumb.png';
import styled from 'styled-components';
import {bgColor} from '../../config/Theme';

const Slider = styled.Slider`
  width: 270;
  height: 100;
`;

export default function CountSlider({counter, setCounter}) {
  return (
    <Slider
      minimumValue={0}
      maximumValue={99}
      step={3}
      minimumTrackTintColor={bgColor.secondary}
      maximumTrackTintColor={bgColor.primary}
      onValueChange={value => setCounter(value)}
      thumbTintColor={bgColor.secondary}
      value={counter}
      // thumbImage={sliderThumb}
    />
  );
}
