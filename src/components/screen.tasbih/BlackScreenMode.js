/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import {CountContext} from './CountContextProvider';

export default function BlackScreenMode(props) {
  const countContext = useContext(CountContext);
  const {count, setCount} = countContext;
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#000',
        width: 1000,
        height: 100,
      }}>
      <Text> backgroundColor</Text>
      <TouchableOpacity
        style={{
          flex: 1,
          backgroundColor: '#000',
          width: 1000,
          height: 100,
        }}
        onPress={() => setCount(count + 1)}
      />
    </View>
  );
}
