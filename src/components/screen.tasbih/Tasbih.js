import React, {useState, useContext} from 'react';
import {View, StyleSheet, Text, Vibration} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import CounterButton from './CounterButton';
import CountSlider from './CountSlider';
import ResetCount from './ResetCount';
import FlexRowCentred from '../elements/FlexRowCentred';
import {VIBRATION_PERIOD} from '../../config/Vars';
import {textColor, bgColor} from '../../config/Theme';
import ActionButton from '../elements/ActionButton';
import {CountContext} from './CountContextProvider';

export default function Tasbih(props) {
  const countContext = useContext(CountContext);
  const {count, setCount} = countContext;
  const [counter, setCounter] = useState(0);

  const navigation = useNavigation();

  const vib = () => Vibration.vibrate(VIBRATION_PERIOD);
  const intVib = () => Vibration.vibrate(100);
  count > 0 && count % 33 === 0 && intVib();
  count > 0 && counter === count && vib();

  return (
    <View style={styles.root}>
      <ActionButton
        width={100}
        height={50}
        bgColor={bgColor.primary}
        action={'Black'}
        borderRadius={7}
        elevation={7}
        onPress={() => {
          return navigation.navigate('blackScreenMode');
        }}
      />

      <View style={styles.viewCount}>
        <Text style={styles.textCount}>
          {count} of {counter}
        </Text>
      </View>

      <View style={styles.viewCounter}>
        <CounterButton setCount={setCount} count={count} counter={counter} />
      </View>

      <FlexRowCentred endFlex={0.05} style={styles.viewReset}>
        <ResetCount setCount={setCount} setCounter={setCounter} count={count} />
      </FlexRowCentred>
      <View style={styles.viewSlider}>
        <CountSlider counter={counter} setCounter={setCounter} />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: bgColor.bg,
  },
  viewBtn: {
    flex: 1,
    justifyContent: 'center',
  },
  textCount: {
    fontSize: 70,
    color: textColor.secondary,
    textAlign: 'center',
  },
  viewCount: {
    flex: 2,
    alignSelf: 'center',
  },
  viewCounter: {
    flex: 5,
    alignSelf: 'center',
  },
  viewReset: {
    flex: 2,
    alignSelf: 'flex-start',
  },
  viewSlider: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
