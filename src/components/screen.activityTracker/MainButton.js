import React from 'react';
import {TouchableOpacity, Text, StyleSheet, Animated} from 'react-native';
import moment from 'moment';
import {bgColor} from '../../config/Theme';
export default function MainButton({useTimerMethods}) {
  const {isRunning, elapsedTime, startTimer, pauseTimer} = useTimerMethods;

  const runningTime = moment.utc(elapsedTime).format('HH:mm:ss');

  const animatedOpacity = new Animated.Value(1);
  const blinker = toValue => {
    if (!isRunning) {
      Animated.timing(animatedOpacity, {
        toValue,
        duration: 500,
      }).start(() => blinker(toValue === 1 ? 0 : 1));
    } else {
      Animated.timing(animatedOpacity, {
        toValue: 1,
        duration: 500,
      }).start();
    }
  };
  blinker(0);

  if (elapsedTime > 0) {
    return (
      <TouchableOpacity
        style={[
          styles.runningBtn,
          {backgroundColor: isRunning ? bgColor.secondary : bgColor.primary},
        ]}
        onPress={() => (isRunning ? pauseTimer() : startTimer())}>
        <Animated.View style={{opacity: animatedOpacity}}>
          <Text style={styles.timerText}>{runningTime}</Text>
        </Animated.View>
        {isRunning ? (
          <Text style={styles.pauseResumeText}>PAUSE</Text>
        ) : (
          <Text style={styles.pauseResumeText}>RESUME</Text>
        )}
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity style={styles.startBtn} onPress={() => startTimer()}>
      <Text style={styles.btnText}>START</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  startBtn: {
    backgroundColor: bgColor.primary,
    width: 300,
    height: 300,
    borderRadius: 150,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 70,
    fontWeight: '700',
    color: 'white',
  },
  runningBtn: {
    backgroundColor: bgColor.primary,
    width: 300,
    height: 300,
    borderRadius: 150,

    alignItems: 'center',
  },
  timerText: {
    fontSize: 70,
    fontWeight: '700',
    color: 'white',
    marginTop: 100,
  },
  pauseResumeText: {
    fontSize: 30,
    fontWeight: '700',
    color: 'white',
    marginTop: 50,
  },
});
