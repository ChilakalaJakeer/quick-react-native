/* eslint-disable radix */
import React, {useEffect} from 'react';
import {View, StyleSheet, AppState} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import MainButton from './MainButton';
import Finish from './Finish';
import {useTimer} from './customHooks';
import {
  ELAPSED_TIME_KEY,
  IS_RUNNING_KEY,
  APP_STATE_KEY,
} from '../../config/async-storageKeys';
import {bgColor} from '../../config/Theme';
export default function ActivityTracker(props) {
  const {
    isRunning,
    setIsRunning,
    elapsedTime,
    setElapsedTime,
    startTimer,
    pauseTimer,
    resetTimer,
  } = useTimer();
  const useTimerMethods = {
    isRunning,
    setIsRunning,
    elapsedTime,
    setElapsedTime,
    startTimer,
    pauseTimer,
    resetTimer,
  };
  useEffect(() => {
    AppState.addEventListener('change', handleAppState);
    return () => AppState.removeEventListener('change', handleAppState);
  });
  async function handleAppState(nextAppState) {
    console.log('nextAppState', nextAppState);
    const timeAtAppStateChange = new Date().getTime();
    const readElapsedTime = parseInt(
      await AsyncStorage.getItem(ELAPSED_TIME_KEY),
    );
    const readTimeAtAppStateChange = parseInt(
      await AsyncStorage.getItem(APP_STATE_KEY),
    );
    const readIsRunning = await AsyncStorage.getItem(IS_RUNNING_KEY);
    console.log('read async data :', readElapsedTime, readTimeAtAppStateChange);
    if (
      !isNaN(readElapsedTime) &&
      nextAppState === 'active' &&
      readElapsedTime > 0
    ) {
      const now = new Date().getTime();
      const updatedTime = readElapsedTime + (now - readTimeAtAppStateChange);

      console.log('inside if ', readIsRunning, updatedTime, now);
      if (readIsRunning === 'true') {
        setElapsedTime(updatedTime);
        setIsRunning(true);
      }
    }
    if (nextAppState === 'background') {
      await AsyncStorage.setItem(ELAPSED_TIME_KEY, elapsedTime.toString());
      await AsyncStorage.setItem(
        APP_STATE_KEY,
        timeAtAppStateChange.toString(),
      );
      await AsyncStorage.setItem(IS_RUNNING_KEY, isRunning ? 'true' : 'false');
    }
  }

  return (
    <View style={styles.root}>
      <View style={styles.mainBtnView}>
        <MainButton useTimerMethods={useTimerMethods} />
      </View>
      <View style={styles.saveBtnView}>
        <Finish
          useTimerMethods={useTimerMethods}
          navigation={props.navigation}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: bgColor.bg,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  mainBtnView: {
    flex: 0.8,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  saveBtnView: {
    flex: 0.2,
  },
});
