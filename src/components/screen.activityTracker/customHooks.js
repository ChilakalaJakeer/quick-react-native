import {useState, useEffect} from 'react';

export const useTimer = () => {
  const [isRunning, setIsRunning] = useState(false);
  const [elapsedTime, setElapsedTime] = useState(0);

  useEffect(() => {
    let intervalId;
    if (isRunning) {
      intervalId = setInterval(
        () => setElapsedTime(prevElapsedTime => prevElapsedTime + 1000),
        1000,
      );
    }
    return () => clearInterval(intervalId);
  }, [isRunning]);
  const startTimer = () => setIsRunning(true);
  const pauseTimer = () => setIsRunning(false);
  const resetTimer = () => {
    setIsRunning(false);
    return setElapsedTime(0);
  };

  return {
    isRunning,
    setIsRunning,
    elapsedTime,
    setElapsedTime,
    startTimer,
    pauseTimer,
    resetTimer,
  };
};
