import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {bgColor} from '../../config/Theme';

export default function Finish({useTimerMethods}) {
  const navigation = useNavigation();

  const {elapsedTime, resetTimer} = useTimerMethods;
  if (elapsedTime > 0) {
    return (
      <TouchableOpacity
        style={styles.root}
        onPress={() => {
          const timeTaken = elapsedTime;
          resetTimer();
          return navigation.navigate('save', {timeTaken});
        }}>
        <View style={styles.view}>
          <Text style={styles.text}> FINISH</Text>
        </View>
      </TouchableOpacity>
    );
  }
  return null;
}
const styles = StyleSheet.create({
  root: {
    width: 250,
    height: 80,
    elevation: 7,
  },
  view: {
    flex: 1,
    alignItems: 'center',
  },
  text: {
    fontSize: 70,
    color: bgColor.primary,
  },
});
