import React, {useEffect, useState} from 'react';
import {ScrollView, View, Text, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FlexRowCentred from '../elements/FlexRowCentred';
import ListItem from './ListItem';
import {ACTIVITY_LIST_KEY} from '../../config/async-storageKeys';
import {bgColor} from '../../config/Theme';

export default function History(props) {
  const isFocused = useIsFocused();
  const [activities, setActivities] = useState([]);
  const [clearActivities, setClearActivities] = useState(false);
  const data = async () => {
    let actvityList = await AsyncStorage.getItem(ACTIVITY_LIST_KEY);
    const parsedActvities = JSON.parse(actvityList);
    return setActivities(parsedActvities);
  };

  const removeActivities = async () => {
    setClearActivities(!clearActivities);
    return await AsyncStorage.removeItem(ACTIVITY_LIST_KEY);
  };
  useEffect(() => {
    isFocused && data();
  }, [isFocused, clearActivities]);

  console.table(activities);
  const ClearAllIcon = () => <Icon name={'delete'} size={27} />;

  return (
    <View style={styles.root}>
      <View style={styles.headerView}>
        <FlexRowCentred endFlex={1}>
          <FlexRowCentred endFlex={1}>
            <Text style={styles.headerText}>Saved actvities</Text>
          </FlexRowCentred>
          <FlexRowCentred
            endFlex={1}
            style={{alignItems: 'center', paddingStart: 30}}>
            <TouchableOpacity onPress={() => removeActivities()}>
              <Text>
                <ClearAllIcon />
              </Text>
            </TouchableOpacity>
          </FlexRowCentred>
        </FlexRowCentred>
      </View>
      <ScrollView styles={{flex: 1}}>
        <View style={styles.scrollView}>
          {activities !== [] && activities !== null ? (
            activities.map(actvity => (
              <ListItem actvity={actvity} key={actvity.id} />
            ))
          ) : (
            <Text
              style={{
                fontSize: 40,
                textAlign: 'center',
                color: '#0003',
              }}>
              No actvities{' '}
            </Text>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {flex: 1, backgroundColor: bgColor.bg},
  headerView: {
    height: 50,
    borderBottomWidth: 1,
    borderColor: bgColor.primary,
    backgroundColor: bgColor.primary,
    opacity: 0.5,
  },
  headerText: {fontSize: 40, textAlign: 'center'},
  scrollView: {
    flex: 1,
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
  },
});
