import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import moment from 'moment';

import FlexRowCentred from '../elements/FlexRowCentred';

export default function ListItem(props) {
  const {actvityName, timeTaken, timeStamp} = props.actvity;
  const time = moment.utc(timeTaken).format('HH:mm:ss');
  const savedDate = moment.utc(timeStamp).format('DD MMM YYYY');
  const actvity = {actvityName, time, savedDate};
  //   const actvity = {
  //     actvityName: 'actvityName',
  //     timeTaken: '00:00:01',
  //     timeStamp: '12 sept 1999',
  //   };
  return (
    <FlexRowCentred endFlex={0.1} style={styles.root}>
      <View style={styles.listItem}>
        <View style={styles.actvityNameView}>
          <Text style={styles.actvityName}>{actvity.actvityName}</Text>
        </View>
        <View style={styles.timeView}>
          <Text style={styles.timeTaken}>{actvity.time}</Text>
          <Text style={styles.timeStamp}>{actvity.savedDate}</Text>
        </View>
      </View>
    </FlexRowCentred>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingVertical: 7,
    borderColor: '#0005',
    borderBottomWidth: 1,
  },
  listItem: {
    flexDirection: 'row',
    flex: 1,
  },
  actvityNameView: {flex: 1, alignItems: 'flex-start'},
  timeView: {flex: 1, alignItems: 'flex-end'},
  actvityName: {fontSize: 24},
  timeTaken: {fontSize: 16},
  timeStamp: {fontSize: 16, color: '#0005'},
});
