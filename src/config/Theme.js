export const bgColor = {
  bg: '#fee',
  primary: '#534d8a',
  secondary: '#843972',
};
export const textColor = {
  primary: '#fff',
  secondary: '#000',
};
