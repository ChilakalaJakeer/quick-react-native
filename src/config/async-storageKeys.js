export const ELAPSED_TIME_KEY = '@elapsedTime';
export const IS_RUNNING_KEY = '@isRunning';
export const APP_STATE_KEY = '@timeAtAppStateChange';
export const ACTIVITY_LIST_KEY = '@actvityList';
