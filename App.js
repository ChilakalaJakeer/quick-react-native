import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ActivityTracker from './src/components/screen.activityTracker/ActivityTracker';
import Save from './src/components/screen.save/Save';
import History from './src/components/screen.history/History';
import Tasbih from './src/components/screen.tasbih/Tasbih';
import AppStatusBar from './src/components/elements/AppStatusBar';
import {bgColor} from './src/config/Theme';
import BlackScreenMode from './src/components/screen.tasbih/BlackScreenMode';
import CountContextProvider from './src/components/screen.tasbih/CountContextProvider';

const Drawer = createDrawerNavigator();
const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

const STATUSBAR_COLOR = bgColor.primary;

function App() {
  const ActivityTrackerIcon = () => <Icon name={'timer'} size={27} />;
  const HistoryIcon = () => <Icon name={'history'} size={27} />;

  const ActivityTrackerStackNav = () => {
    return (
      <Stack.Navigator headerMode={'none'} initialRouteName={'activityTracker'}>
        <Stack.Screen name="activityTracker" component={ActivityTracker} />
        <Stack.Screen name="save" component={Save} />
      </Stack.Navigator>
    );
  };

  const TasbihStackNav = () => {
    return (
      <CountContextProvider>
        <Stack.Navigator headerMode={'none'} initialRouteName={'Tasbih'}>
          <Stack.Screen name="tasbih" component={Tasbih} />
          <Stack.Screen name="blackScreenMode" component={BlackScreenMode} />
        </Stack.Navigator>
      </CountContextProvider>
    );
  };

  const ActivityTrackerTabNav = () => {
    return (
      <Tab.Navigator
        tabBarOptions={{
          labelStyle: {fontSize: 19},
          style: {backgroundColor: bgColor.primary},
          showIcon: true,
          showLabel: false,
        }}>
        <Tab.Screen
          name="activityTracker"
          component={ActivityTrackerStackNav}
          options={{
            tabBarIcon: () => <ActivityTrackerIcon />,
          }}
        />

        <Tab.Screen
          name="history"
          component={History}
          options={{
            tabBarIcon: () => <HistoryIcon />,
          }}
        />
      </Tab.Navigator>
    );
  };

  /*
  const TasbihTabNav = () => {
    return (
      <Tab.Navigator
        headerMode={'none'}
        tabBarOptions={{
          // labelStyle: {fontSize: 19},
          style: {backgroundColor: bgColor.primary},
          showIcon: true,
          showLabel: false,
        }}>
        <Tab.Screen
          headerMode={'none'}
          name="Tasbih"
          component={TasbihStackNav}
          options={{
            tabBarIcon: () => <ActivityTrackerIcon />,
          }}
        />
      </Tab.Navigator>
    );
  };
*/
  return (
    <>
      <NavigationContainer>
        <SafeAreaView style={styles.topSafeArea} />
        <SafeAreaView style={styles.bottomSafeArea}>
          <AppStatusBar
            backgroundColor={STATUSBAR_COLOR}
            barStyle="light-content"
          />

          <Drawer.Navigator>
            <Drawer.Screen name="Tasbih" component={TasbihStackNav} />
            <Drawer.Screen
              name="Activity Tracker"
              component={ActivityTrackerTabNav}
            />
          </Drawer.Navigator>
        </SafeAreaView>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  topSafeArea: {
    flex: 0,
    backgroundColor: STATUSBAR_COLOR,
  },
  bottomSafeArea: {
    flex: 1,
    backgroundColor: STATUSBAR_COLOR,
  },
});
export default App;
